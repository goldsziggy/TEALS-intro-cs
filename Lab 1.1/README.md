# TEALS Lab 0.4

## 3 things SNAP can do

1. build small programs
2. draw shapes
3. Play Sounds

# Lab 1.1

## Blocks

- 2.1
  _ Sound
  _ Control
  _ Sensing
  _ Sensing
  _ Motion
  _ Sensing
  _ Motion
  _ Sound
- 2.2 \* Change the value to a negative

## Scripts

You script will move the sprite 10 units, before saying the word hello

## Reporters

- 4.1
  _ 240
  _ 0 \* -240

## Position

- 5.1 \* -100,-80
- 5.2
  _ Glide * secs to X * Y \_
  _ Move _ steps \* Set x to _ \* change x by \_

## Experiment with Drawing Commands

- 6.1
  _ move 10 units in current direction
  _ turn sprite by 15 degrees to the right
  _ clear drawn
  _ lift pen up, so you can move the pen without drawing \* Put pen on the paper, so you can begin drawing
- 6.2 \* No
- 6.3

```
clear
pen down
move 10 steps
turn 90
move 10 steps
turn 90
move 10 steps
turn 90
move 10 steps
turn 90
pen up
```

## Part 7: Follow that Mouse

- 7.1
  _ Make the sprite follow the mouse
  _ The sprite follows you off the stage \* The sprite is slightly off center of the mouse

## Part 8: Forever and a Day

- 8.1 \* d
- 8.2 \* c
- 8.3 \* c
- 8.4 \* a

## Part 9: Kalediscope

[Snap! Build Your Own Blocks 4.2.1.3](https://snap.berkeley.edu/snapsource/snap.html#present:Username=m.zygowicz&ProjectName=Kaleidoscope%20Starter)
