# TEALS Lab 0.4

## Direct SNAP link

https://snap.berkeley.edu/snapsource/snap.html#present:Username=m.zygowicz&ProjectName=Lab%200.4

## 3 things SNAP can do

1. build small programs
2. draw shapes
3. Play Sounds
4. Change costumes

## Running the snap program

This very basic program will do 2 things.

1. Allow you to move the cat left and right (with animation of the cat).
2. Allow you to hit 'SPACE' key to go through the speech dialog.

### Controls

| Key         | Operation                                |
| ----------- | ---------------------------------------- |
| Right Arrow | Move the character to the right          |
| Left Arrow  | Move the character to the left           |
| Space       | Have the character go through the dialog |
